package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {
    private static WebDriver driver;

    public RegisterPage(WebDriver driver){
        RegisterPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "firstname")
    private WebElement inputFirstName;

    @FindBy(id = "lastname")
    private WebElement inputLastName;

    @FindBy(id = "email_address")
    private WebElement inputEmail;

    @FindBy(id = "password")
    private WebElement inputPassword;

    @FindBy(id = "confirmation")
    private WebElement inputConfirmPassword;

    @FindBy(id = "is_subscribed")
    private WebElement checkSignUpForNewsteller;

    @FindBy(xpath = "//button[@title = 'Register']")
    private WebElement btnRegister;

    public MyAccountPage createAccount(String firstName, String lastName, String email, String password, String confirmPassword) {
        inputFirstName.sendKeys(firstName);
        inputLastName.sendKeys(lastName);
        inputEmail.sendKeys(email);
        inputPassword.sendKeys(password);
        inputConfirmPassword.sendKeys(confirmPassword);
        btnRegister.click();
        return new MyAccountPage(driver);
    }
}