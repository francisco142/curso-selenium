package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage {
    private static WebDriver driver;

    ShoppingCartPage(WebDriver driver){
        ShoppingCartPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "(//span[@class = 'cart-price']//span)[1]")
    private WebElement price;

    @FindBy(xpath = "((//dl[@class = 'item-options'])[2]//following-sibling::dd)[1]")
    private WebElement color;

    @FindBy(xpath = "((//dl[@class = 'item-options'])[2]//following-sibling::dd)[2]")
    private WebElement size;

    @Step("Obteniendo precio del producto")
    public String getPrice(){
        return price.getText();
    }

    @Step("Obteniendo color del producto")
    public String getColor(){
        return color.getText();
    }

    @Step("Obteniendo size del producto")
    public String getSize(){
        return size.getText();
    }
}