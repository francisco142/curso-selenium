package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private static WebDriver driver;

    public LoginPage(WebDriver driver){
        LoginPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(name = "login[username]")
    private WebElement inputEmail;

    @FindBy(id = "pass")
    private WebElement inputPassword;

    @FindBy(id = "send2")
    private WebElement btnLogin;

    public MyAccountPage login(String firstName, String lastName) {
        inputEmail.sendKeys(firstName);
        inputPassword.sendKeys(lastName);
        btnLogin.click();
        return new MyAccountPage(driver);
    }
}