package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class HomePage {
    private static WebDriver driver;

    public HomePage(WebDriver driver){
        HomePage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "search")
    private WebElement searchBar;

    @FindBy(id = "select-language")
    private WebElement selectLanguage;

    @FindBy(xpath = "//span[contains(text(), 'Account')]//parent::a")
    private WebElement linkAccount;

    @FindBy(xpath = "//a[@title = 'Register']")
    private WebElement linkRegister;

    @FindBy(xpath = "//a[@title = 'Log In']")
    private WebElement linkLogin;

    @FindBy(xpath = "//a[@class = 'skip-link skip-account']//span[2]")
    private WebElement labelAccount;

    public ProductSearchPage search(String product){
        searchBar.sendKeys(product);
        searchBar.sendKeys(Keys.ENTER);
        return new ProductSearchPage(driver);
    }

    public void selectLanguage(String idioma){
        selectLanguage.click();
        Actions actions = new Actions(driver);
        Select select = new Select(selectLanguage);
        actions.moveToElement(selectLanguage);
        select.selectByVisibleText(idioma);
    }

    @Step("Haciendo click en el Account")
    public RegisterPage clickRegister(){
        linkAccount.click();
        linkRegister.click();
        return new RegisterPage(driver);
    }

    @Step("Haciendo click Login")
    public LoginPage clickLogin(){
        linkAccount.click();
        linkLogin.click();
        return new LoginPage(driver);
    }

    public String getTextLabelAccount(){
        return labelAccount.getText();
    }



}