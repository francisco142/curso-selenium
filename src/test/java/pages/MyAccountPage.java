package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage {
    private static WebDriver driver;

    public MyAccountPage(WebDriver driver){
        MyAccountPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//span[contains(text(), 'Thank you for registering with Madison Island.')]")
    private WebElement labeBienvenida;

    @FindBy(css = "p.hello")
    private WebElement labelSaludoBienvenida;

    @Step("Obteniendo saludo de bienvenida Madison Island")
    public String getLabelBienvenida(){
        return labeBienvenida.getText();
    }

    @Step("Obteniendo saludo de bienvenida registro")
    public String getLabelSaludoBienvenida(){
        return labelSaludoBienvenida.getText();
    }


}