package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class ProductPage {
    private static WebDriver driver;

    ProductPage(WebDriver driver){
        ProductPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "(//div[@class = 'price-box']//span[contains(@class , 'regular-price')]//span)[1]")
    private WebElement price;

    @FindBy(xpath = "//div/button[@title = 'Add to Cart']")
    private WebElement btnAddToCart;

    @FindBy(id = "attribute92")
    private WebElement comboColor;

    @FindBy(id = "attribute180")
    private WebElement comboSize;

    @Step("Obteniendo precio del producto")
    public String getPrice(){
        return price.getText();
    }

    public ShoppingCartPage clickBtnAddToCart(){
        btnAddToCart.click();
        return new ShoppingCartPage(driver);
    }

    public void seletComboColor(String color) throws InterruptedException {
        comboColor.click();
        Actions actions = new Actions(driver);
        Select select = new Select(comboColor);
        actions.moveToElement(comboColor);
        select.selectByVisibleText(color);
    }

    public void seletComboSize(String size){
        comboSize.click();
        Actions actions = new Actions(driver);
        Select select = new Select(comboSize);
        actions.moveToElement(comboSize);
        select.selectByVisibleText(size);
    }

    @Step("Obteniendo color seleccionado")
    public String getColor(){
        Select select = new Select(comboColor);
        return select.getFirstSelectedOption().getText();
    }

    @Step("Obteniendo size seleccionado")
    public String getSize(){
        Select select = new Select(comboSize);
        return select.getFirstSelectedOption().getText();
    }


}