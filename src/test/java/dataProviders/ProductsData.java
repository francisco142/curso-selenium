package dataProviders;

import org.testng.annotations.DataProvider;

public class ProductsData {
    @DataProvider(name = "products")
    public static Object[][] getProductsData() {
        return new Object[][]{
                {"Shirt", "$125.00"},
                {"TriBeCa", "$185.00"},
        };
    }
}